# Установка
- скачайте из репозитория
`git clone git@bitbucket.org:f1aky927/phone_test.git`
- установите зависимости
```
cd phone_test
pip install -r requirements.txt
```

# Использование
```
from libsms import sms_transport
from libsms import sms_transports

sms_transport.send(phone=’123123’, msg=’qweqwe’)
sms_transports[‘dummy’].send(phone=’123123’, msg=’qweqwe’)
```