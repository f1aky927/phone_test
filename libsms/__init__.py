# -*- encoding: utf-8 -*-

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.functional import SimpleLazyObject
from django.utils.module_loading import import_string

__all__ = ['sms_transport', 'sms_transports']

try:
    config = settings.SMS_TRANSPORTS
except AttributeError:
    raise ImproperlyConfigured(
        u'В Ваших настройках не указаны настройки для libsms.')


class Transports(object):

    def __init__(self):
        self.backends = dict()

        for key, data in config.iteritems():
            klass = import_string(data.get('BAKEND'))
            self.backends[key] = SimpleLazyObject(
                lambda: klass(data.get('PARAMS')))

        if 'default' not in self.backends:
            raise ImproperlyConfigured(u'Не указан backend по умолчанию')

    def __getitem__(self, item):
        return self.backends[item]

sms_transports = Transports()
sms_transport = sms_transports['default']
