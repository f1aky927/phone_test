# -*- encoding: utf-8 -*-

from abc import ABCMeta, abstractmethod


class BaseBackend(object):
    __metaclass__ = ABCMeta

    def __init__(self, params=None):
        self.params = params

    @abstractmethod
    def send(self, phone, msg):
        """Отправка смс"""